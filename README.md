# pca9685-promise

A node.js module for using a PCA9685 (led/servo) controller using
the async-i2c-bus library, a node.js module giving a promisified interface
to the I2C bus.

The standard mode of operation is to create a bus, create a device, open
the bus, initialize the device, set the frequency and configure channels.

The module uses the auto-increment feature of PCA9685 to reduce the volume
of exchanges over the I2C bus.
