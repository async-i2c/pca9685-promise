/* Unit testing files for pca9685 */

import {Bus} from "async-i2c-bus";
import {PCA9685} from "../index";

jest.mock("i2c-bus", () => {
    const createI2cBusMock = require("async-i2c-bus/dist/main/lib/createI2cBusMock").default;
    // The RESTART bit has a weird behavior we try to mimick
    const logic: ProxyHandler<Buffer> = {
        set: (obj: any, prop: string, val: any) => {
            if (prop === "0") {
                if (val & 0x10) {
                    // Set Restart on sleep
                    obj[0] = val | 0x80;
                } else {
                    // Cancel restart only if set.
                    obj[0] = val ^ (0x80 & obj[0]);
                }
            } else {
                obj[prop] = val;
            }
            return true;
    }};
    const buffer1 = Buffer.alloc(0xff, 0);
    const buffer2 = Buffer.alloc(0xff, 0);
    // Initialization to sleep state.
    buffer1[0] = 0x90;
    return {
        open: createI2cBusMock({
            devices: {
                0x40: new Proxy(buffer1, logic),
                0x41: buffer2,
            },
        }),
    };
});

describe("PCA9685", () => {
    it("initialization of device", async () => {
        const bus = Bus({});
        const device = new PCA9685(bus);
        await bus.open();
        const spy = jest.spyOn(bus, "writeByte");
        await device.init();

        expect(spy).toHaveBeenNthCalledWith(1, 0x40, 0, 0x10);
        expect(spy).toHaveBeenNthCalledWith(2, 0x40, 0, 0x30);
        expect(spy).toHaveBeenNthCalledWith(3, 0x40, 1, 0x4);
        expect(spy).toHaveBeenNthCalledWith(4, 0x40, 0xFE, 0x1E);
        /* We can check the restart because of our weird logic. */
        expect(spy).toHaveBeenNthCalledWith(5, 0x40, 0, 0x20);
        expect(spy).toHaveBeenNthCalledWith(6, 0x40, 0, 0xA0);
    });

    it("set the frequency", async () => {
        const bus = Bus({});
        const device = new PCA9685(bus);
        await bus.open();
        await device.init();
        const spy = jest.spyOn(bus, "writeByte");
        await device.set_frequency(50);
        // to sleep (as Adafruit python lib)
        expect(spy).toHaveBeenNthCalledWith(1, 0x40, 0, 0x30);
        // set the freq
        expect(spy).toHaveBeenNthCalledWith(2, 0x40, 0xFE, 121);
        // restarting
        expect(spy).toHaveBeenNthCalledWith(3, 0x40, 0, 0x20);
        expect(spy).toHaveBeenNthCalledWith(4, 0x40, 0, 0xA0);
    });

    it("too high/too low frequency", async () => {
        const bus = Bus({});
        const device = new PCA9685(bus);
        await bus.open();
        await bus.writeByte(0x40, 0, 0x90);
        await device.init();
        expect(device.set_frequency(100000)).rejects.toBeInstanceOf(Error);
        expect(device.set_frequency(10)).rejects.toBeInstanceOf(Error);
    });

    it("Set a PWM", async () => {
        const bus = Bus({});
        // we cannot use proxy because of the copy.
        const device = new PCA9685(bus, {address: 0x41});
        await bus.open();
        await bus.writeByte(0x41, 0, 0x90);
        await device.init();
        const chan = 3;
        await device.set_pwm(chan, 0x0123, 0x0456);
        const b = new Array<number>(4);
        for (let i = 0; i < 4; i++) {
            b[i] = await bus.readByte(0x41, 6 + i + chan * 4);
        }
        expect(b[0] + (b[1] << 8)).toEqual(0x123);
        expect(b[2] + (b[3] << 8)).toEqual(0x456);
    });
});
